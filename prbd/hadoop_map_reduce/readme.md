# LABORATORIUM PRBD – HADOOP 02

## Running on Hadoop
Any of the presented solutions can be run on Hadoop using the following example command:
```bash
student@ubuntu:~$ /usr/local/hadoop/bin/hdfs dfs -cat numbers_input.txt
0 1 1 2 3 5 8 13 21 34 55 89 144 233

student@ubuntu:~$ MAPPER=max_number_mapper.py \
REDUCER=max_number_reducer.py \
/usr/local/hadoop/bin/hadoop jar /usr/local/hadoop/share/hadoop/tools/lib/hadoop-streaming-3.3.0.jar \
-file /home/$USER/$MAPPER \
-mapper /home/$USER/$MAPPER \
-file /home/$USER/$REDUCER \
-reducer /home/$USER/$REDUCER \
-input numbers_input.txt \
-output max_output
(...)
2021-05-22 12:46:53,528 INFO streaming.StreamJob: Output directory: max_output

student@ubuntu:~$ /usr/local/hadoop/bin/hdfs dfs -cat max_output/part-00000
233
```

## Solutions

### 1. Char count
* mapper: [char_count_mapper.py](./char_count_mapper.py)
* reducer: [char_count_reducer.py](./char_count_reducer.py)

```
❯ echo "foo foo quux labs foo bar quux" | ./char_count_mapper.py | sort -k1,1 | ./char_count_reducer.py
a       2
b       2
f       3
l       1
o       6
q       2
r       1
s       1
u       4
x       2
```

### 2. Numbers
#### 2.1. Max value
* mapper: [max_number_mapper.py](./max_number_mapper.py)
* reducer: [max_number_reducer.py](./max_number_reducer.py)
```
❯ echo "0 1 1 2 3 5 8 13 21 34 55 89 144 233" | ./max_number_mapper.py | sort -k1,1 | ./max_number_reducer.py
233
```
#### 2.2. Arithmetic Mean value
* mapper: [arithmetic_mean_mapper.py](./arithmetic_mean_mapper.py)
* reducer: [arithmetic_mean_reducer.py](./arithmetic_mean_reducer.py)
```
❯ echo "0 1 1 2 3 5 8 13 21 34 55 89 144 233" | ./arithmetic_mean_mapper.py | sort -k1,1 | ./arithmetic_mean_reducer.py
43.5
```
#### 2.3. Geometric mean
* mapper: [geometic_mean_mapper.py](./geometic_mean_mapper.py)
* reducer: [geometic_mean_reducer.py](./geometic_mean_reducer.py)
```
❯ echo "1 1 2 3 5 8 13 21 34 55 89 144 233" | ./geometic_mean_mapper.py | sort -k1,1 | ./geometic_mean_reducer.py
13.190330752294596
```
#### 2.4. Distinct numbers count
* mapper: [distinct_numbers_count_mapper.py](./distinct_numbers_count_mapper.py)
* reducer: [distinct_numbers_count_reducer.py](./distinct_numbers_count_reducer.py)
```
❯ echo "0 1 1 2 3 5 8 13 21 34 55 89 144 233" | ./distinct_numbers_count_mapper.py | sort -k1,1 | ./distinct_numbers_count_reducer.py
0       1
1       2
13      1
144     1
2       1
21      1
233     1
3       1
34      1
5       1
55      1
8       1
89      1
```

### 3. Bigrams
* mapper: [bigrams_mapper.py](./bigrams_mapper.py)
* reducer: [bigrams_reducer.py](./bigrams_reducer.py)
```
❯ cat bigram_input.txt | ./bigrams_mapper.py | sort -k1,1 | ./bigrams_reducer.py
(ach *) 1
(ach któryż) 1.0

(bez *) 1
(bez wesoły) 1.0

(biją *) 1
(biją dzwony) 1.0

(bo *) 1
(bo oni) 1.0

(bogiem *) 1
(bogiem o) 1.0

(burz *) 1
(burz widzę) 1.0

(by *) 6
(by chór) 0.16666666666666666
(by dreszczem) 0.16666666666666666
(by hart) 0.3333333333333333
(by jęk) 0.16666666666666666
(by siłę) 0.16666666666666666
(by wciąż) 0.16666666666666666

(był *) 3
(był pełen) 0.3333333333333333
(był w) 0.3333333333333333
(był zawsze) 0.3333333333333333

(całunem *) 1
(całunem czy) 1.0

(chcą *) 2
(chcą by) 0.5
(chcą ode) 0.5

(chór *) 2
(chór już) 0.5
(chór mi) 0.5

(chóru *) 1
(chóru znać) 1.0

(chórze *) 1
(chórze żyli) 1.0

(choć *) 2
(choć idą) 0.5
(choć jesień) 0.5

(chwilę *) 1
(chwilę by) 1.0

(chyba *) 1
(chyba gadał) 1.0

(ciało *) 1
(ciało tylko) 1.0

(ciska *) 1
(ciska groty) 1.0

(co *) 9
(co ciska) 0.1111111111111111
(co leci) 0.1111111111111111
(co legnie) 0.1111111111111111
(co legł) 0.1111111111111111
(co mija) 0.1111111111111111
(co niezatrwożon) 0.1111111111111111
(co skrzydeł) 0.1111111111111111
(co w) 0.1111111111111111
(co zmarł) 0.1111111111111111

(cóż *) 1
(cóż to) 1.0

(czasy *) 1
(czasy burz) 1.0

(czuję *) 1
(czuję bez) 1.0

(czy *) 1
(czy ten) 8.0

(dawno *) 1
(dawno pogrzebiony) 1.0

(do *) 1
(do łez) 1.0

(dość *) 1
(dość już) 1.0

(dreszczem *) 1
(dreszczem wstrząsnął) 1.0

(drogę *) 1
(drogę tnie) 1.0

(duch *) 2
(duch jak) 0.5
(duch niesie) 0.5

(dzisiaj *) 1
(dzisiaj muszę) 1.0

(dzwony *) 1
(dzwony wysoko) 1.0

(dłoni *) 1
(dłoni krzyż) 1.0

(dźwięku *) 1
(dźwięku słów) 1.0

(gadał *) 1
(gadał by) 1.0

(groty *) 1
(groty o) 1.0

(gwiazd *) 1
(gwiazd krzesane) 1.0

(gwiezdny *) 1
(gwiezdny chór) 1.0

(hart *) 1
(hart był) 2.0

(i *) 5
(i całunem) 0.2
(i drogę) 0.2
(i mogiły) 0.2
(i śmieję) 0.2
(i żywych) 0.2

(ich *) 1
(ich wzruszał) 1.0

(idą *) 1
(idą czasy) 1.0

(idę *) 1
(idę w) 1.0

(jak *) 1
(jak ognia) 1.0

(jary *) 1
(jary choć) 1.0

(jesień *) 1
(jesień już) 1.0

(jestem *) 5
(jestem jary) 0.2
(jestem młody) 0.2
(jestem wesoły) 0.2
(jestem świeży) 0.2
(jestem żywy) 0.2

(już *) 5
(już chyba) 0.2
(już idę) 0.2
(już na) 0.2
(już słyszę) 0.4
(już zbywam) 0.2

(jęk *) 1
(jęk był) 1.0

(kirami *) 1
(kirami i) 1.0

(krzemień *) 1
(krzemień gwiazd) 1.0

(krzesane *) 1
(krzesane czy) 1.0

(krzyż *) 1
(krzyż czy) 1.0

(krąg *) 1
(krąg swobody) 1.0

(któryż *) 1
(któryż jestem) 1.0

(kwitnący *) 1
(kwitnący czuję) 1.0

(kwitnących *) 1
(kwitnących ogród) 1.0

(leci *) 1
(leci wzwyż) 1.0

(lecz *) 2
(lecz duch) 0.5
(lecz nie) 0.5

(legnie *) 1
(legnie zmożon) 1.0

(legł *) 1
(legł przykryty) 1.0

(leży *) 1
(leży lecz) 1.0

(loty *) 1
(loty przez) 1.0

(ma *) 1
(ma stanąć) 1.0

(marach *) 1
(marach trup) 1.0

(mi *) 1
(mi odpowiadał) 1.0

(miał *) 1
(miał związane) 1.0

(mija *) 1
(mija szczyty) 1.0

(minie *) 1
(minie chcą) 1.0

(mogiły *) 1
(mogiły lecz) 1.0

(muszę *) 1
(muszę rzec) 1.0

(mym *) 1
(mym chórze) 1.0

(młody *) 1
(młody już) 1.0

(na *) 4
(na chwilę) 0.25
(na marach) 0.25
(na poły) 0.25
(na sąd) 0.25

(nie *) 1
(nie spostrzegli) 1.0

(niebios *) 1
(niebios strop) 1.0

(niech *) 1
(niech wiersze) 1.0

(niesie *) 1
(niesie pełny) 1.0

(niezatrwożon *) 1
(niezatrwożon na) 1.0

(o *) 2
(o chcą) 0.5
(o krzemień) 0.5

(ode *) 1
(ode minie) 1.0

(odpowiadał *) 1
(odpowiadał więc) 1.0

(ognia *) 1
(ognia słup) 1.0

(ogród *) 1
(ogród róż) 1.0

(oni *) 1
(oni się) 1.0

(otuchą *) 1
(otuchą wiary) 1.0

(pełen *) 1
(pełen siły) 1.0

(pełny *) 1
(pełny snop) 1.0

(piec *) 1
(piec bo) 1.0

(piór *) 1
(piór już) 1.0

(piorunem *) 1
(piorunem czy) 1.0

(podniosłą *) 1
(podniosłą łzą) 1.0

(pogrzebiony *) 1
(pogrzebiony duch) 1.0

(poły *) 1
(poły kwitnący) 1.0

(progiem *) 1
(progiem czy) 1.0

(przed *) 1
(przed świątyń) 1.0

(przez *) 1
(przez żywot) 1.0

(przykryty *) 1
(przykryty kirami) 1.0

(róż *) 1
(róż wesoły) 1.0

(rzec *) 1
(rzec tym) 1.0

(rzucą *) 1
(rzucą w) 1.0

(sile *) 1
(sile by) 1.0

(się *) 2
(się do) 0.5
(się gapili) 0.5

(siły *) 1
(siły by) 1.0

(siłę *) 1
(siłę chóru) 1.0

(skrzydeł *) 1
(skrzydeł loty) 1.0

(snadź *) 1
(snadź żem) 1.0

(snop *) 1
(snop ach) 1.0

(spostrzegli *) 1
(spostrzegli snadź) 1.0

(stanąć *) 1
(stanąć z) 1.0

(strop *) 1
(strop trup) 1.0

(swobody *) 1
(swobody już) 1.0

(szczyty *) 1
(szczyty i) 1.0

(szczęśliwy *) 1
(szczęśliwy ściskając) 1.0

(sąd *) 1
(sąd ma) 1.0

(słów *) 1
(słów by) 1.0

(słup *) 1
(słup wesoły) 1.0

(słyszę *) 2
(słyszę biją) 0.5
(słyszę gwiezdny) 0.5

(ten *) 1
(ten co) 8.0

(tnie *) 1
(tnie piorunem) 1.0

(to *) 2
(to ciało) 0.5
(to na) 0.5

(trup *) 2
(trup dawno) 0.5
(trup to) 0.5

(tylko *) 1
(tylko leży) 1.0

(tym *) 1
(tym co) 1.0

(w *) 7
(w dłoni) 0.14285714285714285
(w dźwięku) 0.14285714285714285
(w krąg) 0.14285714285714285
(w mym) 0.14285714285714285
(w niebios) 0.14285714285714285
(w piec) 0.14285714285714285
(w sile) 0.14285714285714285

(wciąż *) 1
(wciąż ich) 1.0

(wesoły *) 2
(wesoły i) 0.5
(wesoły jestem) 2.0

(wiary *) 1
(wiary kwitnących) 1.0

(widzę *) 1
(widzę z) 1.0

(wiersze *) 1
(wiersze rzucą) 1.0

(więc *) 1
(więc dzisiaj) 1.0

(wstrząsnął *) 1
(wstrząsnął znów) 1.0

(wysoko *) 1
(wysoko w) 1.0

(wzruszał *) 1
(wzruszał łzą) 1.0

(wzwyż *) 1
(wzwyż czy) 1.0

(własnych *) 1
(własnych progiem) 1.0

(z *) 2
(z bogiem) 0.5
(z otuchą) 0.5

(zawsze *) 1
(zawsze w) 1.0

(zbytnich *) 1
(zbytnich piór) 1.0

(zbywam *) 1
(zbywam zbytnich) 1.0

(zmarł *) 1
(zmarł szczęśliwy) 1.0

(zmożon *) 1
(zmożon przed) 1.0

(znać *) 1
(znać by) 1.0

(znów *) 1
(znów i) 1.0

(związane *) 1
(związane czy) 1.0

(ściskając *) 1
(ściskając w) 1.0

(żem *) 1
(żem dość) 1.0

(łez *) 1
(łez choć) 1.0

(śmieję *) 1
(śmieję się) 1.0

(świeży *) 1
(świeży cóż) 1.0

(świątyń *) 1
(świątyń własnych) 1.0

(żyli *) 1
(żyli niech) 1.0

(żywot *) 1
(żywot miał) 1.0

(żywy *) 1
(żywy czy) 1.0

(żywych *) 1
(żywych i) 1.0
```
