#!/usr/bin/env python3
"""reducer.py"""

from operator import itemgetter
import sys

numbers_count = 0
total_product = 1

for line in sys.stdin:
    line = line.strip()
    number = int(line)
    total_product = total_product * number
    numbers_count += 1

# print(total_product)
# print(numbers_count)
print(pow(total_product, 1/numbers_count))
