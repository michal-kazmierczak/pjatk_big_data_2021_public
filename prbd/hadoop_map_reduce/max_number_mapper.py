#!/usr/bin/env python3
"""mapper.py"""

import sys
import math

for line in sys.stdin:
    line = line.strip()
    numbers = line.split()

    found_max = -math.inf
    for number in numbers:
        number = int(number)
        if number > found_max:
            found_max = number

    print(found_max)
