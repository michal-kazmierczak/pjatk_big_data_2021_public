#!/usr/bin/env python3
"""reducer.py"""

from operator import itemgetter
import sys

numbers_count = 0
total_sum = 0

for line in sys.stdin:
    line = line.strip()
    number = int(line)
    total_sum = total_sum + number
    numbers_count += 1

# print(total_sum)
# print(numbers_count)
print(total_sum/numbers_count)
