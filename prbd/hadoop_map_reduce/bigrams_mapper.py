#!/usr/bin/env python3
"""mapper.py"""

import sys
import re

previous_word = None

# input comes from STDIN (standard input)
for line in sys.stdin:
    # remove leading and trailing whitespace
    line = line.strip()
    # split the line into words
    words = line.split()

    for word in words:
        # lowercase
        word = word.lower()
        # remove all non-alphanumerical characters
        word = re.sub(r'\W+', '', word)
        # skip words which appear to be empty after transformations
        if word == '':
            continue

        if previous_word:
            print(previous_word + '\t' + word + '\t1')

        previous_word = word
