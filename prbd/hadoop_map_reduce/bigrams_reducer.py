#!/usr/bin/env python3
"""reducer.py"""

from operator import itemgetter
import sys

memorized_first_word = None
memorized_second_word = None
memorized_second_word_count = 0
temp_output = []

def display_temp_output(temp_output):
    rows_count = len(temp_output)
    print('(' + temp_output[0][0] + ' *) ' + str(rows_count))
    for row in temp_output:
        print('(' + row[0] + ' ' + row[1] + ')' + ' ' + str(row[2] / rows_count))
    print()

# input comes from STDIN
for line in sys.stdin:
    # remove leading and trailing whitespace
    line = line.strip()

    # parse the input we got from mapper.py
    first_word, second_word, _count = line.split('\t')

    if not memorized_first_word:
        memorized_first_word = first_word
    if not memorized_second_word:
        memorized_second_word = second_word

    if first_word == memorized_first_word:
        if second_word == memorized_second_word:
            memorized_second_word_count += 1
        else:
            temp_output.append(
                [memorized_first_word, memorized_second_word, memorized_second_word_count]
            )
            memorized_second_word = second_word
            memorized_second_word_count = 1
    else:
        temp_output.append(
            [memorized_first_word, memorized_second_word, memorized_second_word_count]
        )
        display_temp_output(temp_output)
        memorized_first_word = first_word
        memorized_second_word = second_word
        memorized_second_word_count = 1
        temp_output = []
