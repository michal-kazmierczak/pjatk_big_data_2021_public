from mrjob.job import MRJob
import re

class MREntrypoint(MRJob):

    def mapper(self, _, line):
        previous_word = None
        # remove leading and trailing whitespace
        line = line.strip()
        # split the line into words
        words = line.split()

        for word in words:
            # lowercase
            word = word.lower()
            # remove all non-alphanumerical characters
            word = re.sub(r'\W+', '', word)
            # skip words which appear to be empty after transformations
            if word == '':
                continue

            if previous_word:
                yield(previous_word, word)

            previous_word = word

    def reducer(self, first_word, second_word):
        memorized_first_word = None
        memorized_second_word = None
        memorized_second_word_count = 0
        temp_output = []

        if not memorized_first_word:
            memorized_first_word = first_word
        if not memorized_second_word:
            memorized_second_word = second_word

        if first_word == memorized_first_word:
            if second_word == memorized_second_word:
                memorized_second_word_count += 1
            else:
                temp_output.append(
                    [memorized_first_word, memorized_second_word, memorized_second_word_count]
                )
                memorized_second_word = second_word
                memorized_second_word_count = 1
        else:
            temp_output.append(
                [memorized_first_word, memorized_second_word, memorized_second_word_count]
            )
            rows_count = len(temp_output)
            yield(temp_output[0][0] + ' *', str(rows_count))
            for row in temp_output:
                yield(row[0] + ' ' + row[1], str(row[2] / rows_count))

            memorized_first_word = first_word
            memorized_second_word = second_word
            memorized_second_word_count = 1
            temp_output = []

if __name__ == '__main__':
   MREntrypoint.run()
