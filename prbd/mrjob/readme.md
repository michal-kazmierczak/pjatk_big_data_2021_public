# LABORATORIUM PRBD – MR JOB

### 1. Most/least used words with "stop_words

#### 1.2 Most used
```
❯ python3 20_most_used_words.py ./5000-8-U.txt -r local -q
"the"
"of"
"and"
"in"
"to"
"a"
"is"
"it"
"that"
"which"
"as"
"on"
"by"
"be"
"this"
"with"
"are"
"from"
"will"
"at"
```

#### 1.2 Least used
```
❯ python3 20_least_used_words_with_stop_words.py ./5000-8-U.txt -r local -q
"'_channa_'"
"'_codex"
"'_come"
"'_come_'"
"'_congregatione"
"'_essai"
"'_incominciai_'"
"'_lago"
"'_li"
"'_liber"
"'_niente_'"
"'_notizie"
"'_olmo_'"
"'_ordine"
"'_orizonte_'"
"'_orologio"
"'_parceque"
"'_pariete"
"'_proemio_'"
"'_se"
```

### 2. 20 Most/least used digits with stop words

### 2.1 Most used digits
```
❯ python3 20_most_used_digits_with_stop_words.py ./5000-8-U.txt -r local -q
"1"
"2"
"4"
"3"
"0"
"5"
"6"
"8"
"7"
"9"
```

### 2.2 Least used digits
```
❯ python3 20_least_used_digits_with_stop_words.py ./5000-8-U.txt -r local -q
"9"
"7"
"8"
"6"
"5"
"0"
"3"
"4"
"2"
"1"
```

### 2.3 Most used numbers
```
❯ python3 20_most_used_numbers_with_stop_words.py ./5000-8-U.txt -r local -q
"2"
"1"
"4"
"3"
"6"
"5"
"8"
"7"
"9"
"10"
"12"
"20"
"11"
"13"
"14"
"16"
"15"
"40"
"30"
"18"
```

### 2.4 Least used numbers
```
❯ python3 20_least_used_numbers_with_stop_words.py ./5000-8-U.txt -r local -q
"00"
"01"
"03"
"09"
"1002"
"1003"
"1005"
"1006"
"1007"
"1011"
"1012"
"1017"
"1018"
"1023"
"1026"
"1027"
"1032"
"1036"
"1038"
"1039"
```

### 4. Bigrams
* file: [bigrams.py](./bigrams.py)

```
❯ echo "foo foo quux labs foo bar quux" | ./char_count_mapper.py | sort -k1,1 | ./char_count_reducer.py
a       2
b       2
f       3
l       1
o       6
q       2
r       1
s       1
u       4
x       2
```
