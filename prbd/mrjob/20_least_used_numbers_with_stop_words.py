# 20 the most used words excluding "stop_words"
import re
from heapq import nsmallest

from mrjob.job import MRJob
from mrjob.protocol import JSONValueProtocol
from mrjob.step import MRStep

# THE ONLY DIFFERENCE COMPARED TO "20_least_used_digits_with_stop_words"
DIGIT_RE = re.compile(r'[0-9]+')


class MR20LeastUsedNumbersStop(MRJob):
    count = 0
    FILES = ['stop_words.txt']

    OUTPUT_PROTOCOL = JSONValueProtocol

    def configure_args(self):
        super(MR20LeastUsedNumbersStop, self).configure_args()

        # allow for alternate stop words file
        self.add_file_arg(
            '--stop-words-file',
            dest='stop_words_file',
            default=None,
            help='alternate stop words file. lowercase words, one per line',
        )

    def mapper_init(self):
        stop_words_path = self.options.stop_words_file or 'stop_words.txt'

        with open(stop_words_path) as f:
            self.stop_words = set(line.strip() for line in f)

    def mapper_get_digits(self, _, line):
        # yield each word in the line
        for digit in DIGIT_RE.findall(line):
            if digit not in self.stop_words:
                yield (digit, 1)

    def combiner_count_words(self, word, counts):
        # sum the words we've seen so far
        yield (word, sum(counts))

    def reducer_count_words(self, word, counts):
        # send all (num_occurrences, word) pairs to the same reducer.
        # num_occurrences is so we can easily use Python's max() function.
        yield None, (sum(counts), word)

    # discard the key; it is just None
    def reducer_least_top_20_word(self, _, word_count_pairs):
    	for val in nsmallest(20, word_count_pairs):
            yield val

    def steps(self):
        return [
            MRStep(mapper_init=self.mapper_init,
                   mapper=self.mapper_get_digits,
                   combiner=self.combiner_count_words,
                   reducer=self.reducer_count_words),
            MRStep(reducer=self.reducer_least_top_20_word)
        ]


if __name__ == '__main__':
    MR20LeastUsedNumbersStop.run()
